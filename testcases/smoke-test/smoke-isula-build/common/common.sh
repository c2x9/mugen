#!/usr/bin/bash

# Copyright (c) 2023.Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   zhengyiwen
# @Contact   :   470257028@qq.com
# @Date      :   2023-10-12
# @License   :   Mulan PSL v2
# @Desc      :   isula-build test common
# ############################################
# shellcheck disable=SC2034,SC2119,SC1091,SC2154
source "${OET_PATH}"/libs/locallibs/common_lib.sh

function pre_isula_build_env() {
    DNF_INSTALL "isula-build docker-runc iSulad"

    if grep -i version= /etc/os-release | awk -F '"' '{print$2}' | grep "("; then
        os_version=$(grep -i version= /etc/os-release | awk -F '"' '{print$2}' | tr '()' '- ' | sed s/[[:space:]]//g)
    else
        if grep -i version= /etc/os-release | awk -F '"' '{print$2}' | grep "LTS"; then
            os_version=$(grep -i version= /etc/os-release | awk -F '"' '{print$2}' | awk -F ' ' '{print$1"-"$2}')
        else
            os_version=$(grep -i version= /etc/os-release | awk -F '"' '{print$2}')
        fi
    fi
    wget --no-check-certificate -q -P ../common/ \
        https://repo.openeuler.org/openEuler-"${os_version}"/docker_img/"${NODE1_FRAME}"/openEuler-docker."${NODE1_FRAME}".tar.xz

    isula-build ctr-img load -i ../common/openEuler-docker."${NODE1_FRAME}".tar.xz
    images_name=$(isula-build ctr-img images | grep "docker.io/library/openeuler" | awk '{print$1}')
}

function clean_isula_build_env() {
    rm -rf ../common/openEuler-docker."${NODE1_FRAME}".tar.xz
    isula-build ctr-img rm -a
    DNF_REMOVE
}
