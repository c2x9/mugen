#!/bin/sh

set -e

export LC_ALL=C
unset LANG
unset LANGUAGE
. common/config.sh

USE_PAM="yes"
FAILURE_TESTS="yes"

succeeded=0
failed=0
failed_tests=""

run_test()
{
	[ -f RUN_TEST.STOP ] && exit 1

	if $1 > $1.log
	then
		succeeded=$((succeeded+1))
		echo -n "+"
	else
		failed=$((failed+1))
		failed_tests="$failed_tests $1"
		echo -n "-"
	fi
	cat $1.log >> testsuite.log
	[ -f /etc/passwd.lock ] && echo $1 /etc/passwd.lock || true
	[ -f /etc/group.lock ] && echo $1 /etc/group.lock || true
	[ -f /etc/shadow.lock ] && echo $1 /etc/shadow.lock || true
	[ -f /etc/gshadow.lock ] && echo $1 /etc/gshadow.lock || true
	if [ "$(stat -c"%G" /etc/shadow)" != "shadow" ]
	then
		echo $1
		ls -l /etc/shadow
		chgrp shadow /etc/shadow
	fi
	if [ -d /nonexistent ]
	then
		echo $1 /nonexistent
		rmdir /nonexistent
	fi
}

echo "+: test passed"
echo "-: test failed"

# Empty the complete log.
> testsuite.log

run_test ./su/02/env_FOO-options_--login
run_test ./su/02/env_FOO-options_--login_bash
run_test ./su/02/env_FOO-options_--preserve-environment
run_test ./su/02/env_FOO-options_--preserve-environment_bash
run_test ./su/02/env_FOO-options_-
run_test ./su/02/env_FOO-options_-_bash
run_test ./su/02/env_FOO-options_-l-m
run_test ./su/02/env_FOO-options_-l-m_bash
run_test ./su/02/env_FOO-options_-l
run_test ./su/02/env_FOO-options_-l_bash
run_test ./su/02/env_FOO-options_-m_bash
run_test ./su/02/env_FOO-options_-m
run_test ./su/02/env_FOO-options_-p
run_test ./su/02/env_FOO-options_-p_bash
run_test ./su/02/env_FOO-options__bash
run_test ./su/02/env_FOO-options_
run_test ./su/02/env_FOO-options_-p-
run_test ./su/02/env_FOO-options_-p-_bash
run_test ./su/02/env_special-options_-l-p
run_test ./su/02/env_special-options_-l
run_test ./su/02/env_special-options_-l-p_bash
run_test ./su/02/env_special-options_-l_bash
run_test ./su/02/env_special-options_-p
run_test ./su/02/env_special-options_-p_bash
run_test ./su/02/env_special-options_
run_test ./su/02/env_special-options__bash
run_test ./su/02/env_special_root-options_-l-p
run_test ./su/02/env_special_root-options_-l-p_bash
run_test ./su/02/env_special_root-options_-l
run_test ./su/02/env_special_root-options_-l_bash
run_test ./su/02/env_special_root-options_-p
run_test ./su/02/env_special_root-options_-p_bash
run_test ./su/02/env_special_root-options_
run_test ./su/02/env_special_root-options__bash

run_test ./su/03/su_run_command06.test

run_test ./su/04/su_wrong_user.test

run_test ./su/08/env_special-options_
run_test ./su/08/env_special_root-options_
run_test ./su/09/env_special-options_
run_test ./su/09/env_special_root-options_

run_test ./su/12_su_child_failure/su.test
run_test ./su/13_su_child_success/su.test

run_test ./log/faillog/01_faillog_no_faillog/faillog.test
run_test ./log/faillog/02_faillog_usage/faillog.test
run_test ./cktools/01/run1

run_test ./login/01_login_prompt/login.test
run_test ./login/02_login_user/login.test

echo
echo "$succeeded test(s) passed"
echo "$failed test(s) failed"
echo "log written in 'testsuite.log'"
if [ "$failed" != "0" ]
then
	echo "the following tests failed:"
	echo $failed_tests
fi

