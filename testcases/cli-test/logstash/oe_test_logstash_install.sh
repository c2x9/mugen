#!/usr/bin/bash

# Copyright (c) 2023. Huawei Technologies Co.,Ltd.ALL rights reserved.
# This program is licensed under Mulan PSL v2.
# You can use it according to the terms and conditions of the Mulan PSL v2.
#          http://license.coscl.org.cn/MulanPSL2
# THIS PROGRAM IS PROVIDED ON AN "AS IS" BASIS, WITHOUT WARRANTIES OF ANY KIND,
# EITHER EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO NON-INFRINGEMENT,
# MERCHANTABILITY OR FIT FOR A PARTICULAR PURPOSE.
# See the Mulan PSL v2 for more details.

# #############################################
# @Author    :   sunqingwei
# @Contact   :   sunqingwei@uniontech.com
# @Date      :   2023/05/30
# @License   :   Mulan PSL v2
# @Desc      :   logstash install test
# #############################################

source "$OET_PATH/libs/locallibs/common_lib.sh"

function pre_test() {
  LOG_INFO "Start to prepare the test environment."
  DNF_INSTALL "java-1.8.0-openjdk expect"
  mkdir /tmp/test
  cp ./common/logstash-6.6.1.tar.gz /tmp/test
  LOG_INFO "Finish preparing the test environment."
}

function run_test() {
  LOG_INFO "Start to run test."
  tar -xvf /tmp/test/logstash-6.6.1.tar.gz -C /tmp/test
  CHECK_RESULT $? 0 0 "Unzip file failed"
  cp ./common/test.sh /tmp/test/logstash-6.6.1/bin
  cd /tmp/test/logstash-6.6.1/bin || return
  bash test.sh
  grep '\"message\"' test.log | grep '\"hello world\"'
  CHECK_RESULT $? 0 0 "Test failed"
  LOG_INFO "End of the test."
}

function post_test() {
  LOG_INFO "Start to restore the test environment."
  rm -rf /tmp/test
  DNF_REMOVE "$@"
  LOG_INFO "Finish restoring the test environment."
}

main "$@"
